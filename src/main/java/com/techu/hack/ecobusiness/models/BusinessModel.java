package com.techu.hack.ecobusiness.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection= "business")
public class BusinessModel {

    @Id
    String id;
    String name;
    Date registrationDate;
    SectorModel sector;
    String additionalInfo;
    List<EcoCheckModel> checks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public SectorModel getSector() {
        return sector;
    }

    public void setSector(SectorModel sector) {
        this.sector = sector;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public List<EcoCheckModel> getChecks() {
        return checks;
    }

    public void setChecks(List<EcoCheckModel> checks) {
        this.checks = checks;
    }
}
