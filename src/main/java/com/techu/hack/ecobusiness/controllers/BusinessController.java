package com.techu.hack.ecobusiness.controllers;


import com.techu.hack.ecobusiness.models.BusinessModel;
import com.techu.hack.ecobusiness.services.BusinessService;
import com.techu.hack.ecobusiness.services.BusinessServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ecobusiness/v1")
public class BusinessController {

    @Autowired
    BusinessService businessService;

    @GetMapping("/business")
    public ResponseEntity<BusinessServiceResponse> listBusiness (){
        System.out.println("listBusiness");

        return new ResponseEntity<>(new BusinessServiceResponse(), HttpStatus.OK);
    }

    @PostMapping("/business")
    public ResponseEntity<BusinessServiceResponse> createBusiness (@RequestBody BusinessModel newBusiness){
        System.out.println("createBusiness");

        return new ResponseEntity<>(new BusinessServiceResponse(newBusiness), HttpStatus.OK);
    }

    @PostMapping("/business")
    public ResponseEntity<BusinessServiceResponse> addBusiness (@RequestBody BusinessModel newBusiness){

        System.out.println("addBusiness");
        System.out.println("El id de nuevo negocio es: " + newBusiness.getId());
        System.out.println("El nombre de nuevo negocio es: " + newBusiness.getName());


        //Devolvemos un responseEntity con el nuevo negocio y un 201 de respuesta (Created)
        return new ResponseEntity<>(this.businessService.addBusiness(newBusiness), HttpStatus.CREATED);
    }

}
