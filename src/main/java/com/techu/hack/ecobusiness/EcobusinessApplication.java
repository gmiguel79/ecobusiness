package com.techu.hack.ecobusiness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcobusinessApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcobusinessApplication.class, args);
	}

}
