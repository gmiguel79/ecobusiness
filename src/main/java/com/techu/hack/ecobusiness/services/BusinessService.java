package com.techu.hack.ecobusiness.services;

import com.techu.hack.ecobusiness.models.BusinessModel;
import com.techu.hack.ecobusiness.repositories.BusinessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public class BusinessService {

    @Autowired
    BusinessRepository businessRepository;

    public BusinessServiceResponse findAll() {
        System.out.println("findAll");
        return new BusinessServiceResponse();
    }

    public BusinessServiceResponse addBusiness(BusinessModel newBusiness){
        System.out.println("addBusiness");
        BusinessServiceResponse result = new BusinessServiceResponse();

        businessRepository.insert(newBusiness);

        result.setMsg("Negocio añadida correctamente");
        result.setHttpStatus(HttpStatus.CREATED);

        return result;
    }


}