package com.techu.hack.ecobusiness.services;

import org.springframework.http.HttpStatus;

import java.net.http.HttpResponse;

public class ServiceResponse {

    private String msg;
    private HttpStatus httpStatus;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
