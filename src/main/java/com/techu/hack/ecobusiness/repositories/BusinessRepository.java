package com.techu.hack.ecobusiness.repositories;

import com.techu.hack.ecobusiness.models.BusinessModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessRepository extends MongoRepository<BusinessModel, String> {
}
